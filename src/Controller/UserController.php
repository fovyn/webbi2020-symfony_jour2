<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\User;
use App\Form\RegisterType;
use App\Models\User\RegisterForm;
use App\Service\Mapper;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 * @Route(path="/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route(path="/")
     * @param UserService $userService
     * @return Response
     */
    public function listAction(UserService $userService) {
        $users = $userService->readAll();

        return $this->render('user/list.html.twig', ['users', $users]);
    }

    /**
     * @Route(path="/register")
     * @param Request $request
     * @param UserService $userService
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function registerAction(Request $request, UserService $userService, Mapper $mapper) {
        $user = new RegisterForm();

        $form = $this->createForm(RegisterType::class, $user);
        $form->add('submit', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var RegisterForm $data */
            $data = $form->getData();
            $user = $mapper->mapTo(User::class, RegisterForm::class, $data);
//            $user = new User();
//            $user->setLastName($data->getLastName());
//            $user->setFirstName($data->getFirstName());
//            $user->setAddress($data->getAddress());
//            $user->setPassword($data->getPassword());
//            $user->setUserName($data->getUserName());

            $userService->create($user);

            return $this->redirectToRoute('app_user_list');
        }

        return $this->render('user/create.html.twig', ['registerForm' => $form->createView()]);
    }
}
