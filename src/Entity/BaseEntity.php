<?php


namespace App\Entity;


use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BaseEntity
 * @package App\Entity
 * @ORM\MappedSuperclass()
 */
class BaseEntity
{

    /**
     * @var DateTime $createAt
     * @ORM\Column(name="create_at", type="datetime")
     */
    private $createAt;
    /**
     * @var DateTime $updateAt
     * @ORM\Column(name="update_at", type="datetime", nullable=true)
     */
    private $updateAt;
    /**
     * @var boolean $isActive
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @return DateTime
     */
    public function getCreateAt(): DateTime
    {
        return $this->createAt;
    }

    /**
     * @param DateTime $createAt
     * @return BaseEntity
     */
    public function setCreateAt(DateTime $createAt): BaseEntity
    {
        $this->createAt = $createAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdateAt(): DateTime
    {
        return $this->updateAt;
    }

    /**
     * @param DateTime $updateAt
     * @return BaseEntity
     */
    public function setUpdateAt(DateTime $updateAt): BaseEntity
    {
        $this->updateAt = $updateAt;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return BaseEntity
     */
    public function setIsActive(bool $isActive): BaseEntity
    {
        $this->isActive = $isActive;
        return $this;
    }


}