<?php


namespace App\Models\User;


use Symfony\Component\Validator\Constraints as Assert;

class RegisterForm
{
    /**
     * @var string $lastName
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(min="4", max="25", maxMessage="Trop de caractère", minMessage="Pas assez de caractère")
     */
    private $lastName;
    private $firstName;
    /**
     * @var string $userName
     * @Assert\Email()
     */
    private $userName;
    private $address;
    private $password;
    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return RegisterForm
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return RegisterForm
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     * @return RegisterForm
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return RegisterForm
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return RegisterForm
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
}