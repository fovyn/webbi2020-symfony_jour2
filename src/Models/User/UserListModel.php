<?php


namespace App\Models\User;


use App\Entity\Dish;
use App\Entity\Order;
use App\Entity\User;

class UserListModel
{
    /** @var User[] */
    private $users;
    /** @var mixed[] */
    private $orderMap;
    /** @var Dish[] */
    private $dishes;

    public function __construct()
    {
        $this->orderMap = [];
        $this->dishes = [];
    }

    /**
     * @return User[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @param User[] $users
     * @return UserListModel
     */
    public function setUsers(array $users): UserListModel
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return mixed[]
     */
    public function getOrderMap(): array
    {
        return $this->orderMap;
    }

    /**
     * @param mixed[] $orderMap
     * @return UserListModel
     */
    public function setOrderMap(array $orderMap): UserListModel
    {
        $this->orderMap = $orderMap;
        return $this;
    }

    public function addOrder(User $key, Order $value) {
        if (!array_key_exists($key->getUserName(), $this->orderMap)) {
            $this->orderMap[$key->getUserName()] = [];
        }
        $this->orderMap[$key->getUserName()][] = $value;
    }

    public function getOrders(User $key) {
        return $this->orderMap[$key->getUserName()];
    }

    public function getOrderSize(User $key) {
        return sizeof($this->orderMap[$key->getUserName()]);
    }

    /**
     * @return Dish[]
     */
    public function getDishes(): array
    {
        return $this->dishes;
    }

    /**
     * @param Dish[] $dishes
     * @return UserListModel
     */
    public function setDishes(array $dishes): UserListModel
    {
        $this->dishes = $dishes;
        return $this;
    }


}