<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200604100521 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category ADD create_at DATETIME NOT NULL, ADD update_at DATETIME DEFAULT NULL, ADD is_active TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE dish ADD create_at DATETIME NOT NULL, ADD update_at DATETIME DEFAULT NULL, ADD is_active TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE `order` ADD create_at DATETIME NOT NULL, ADD update_at DATETIME DEFAULT NULL, ADD is_active TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE user ADD create_at DATETIME NOT NULL, ADD update_at DATETIME DEFAULT NULL, ADD is_active TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category DROP create_at, DROP update_at, DROP is_active');
        $this->addSql('ALTER TABLE dish DROP create_at, DROP update_at, DROP is_active');
        $this->addSql('ALTER TABLE `order` DROP create_at, DROP update_at, DROP is_active');
        $this->addSql('ALTER TABLE user DROP create_at, DROP update_at, DROP is_active');
    }
}
