<?php


namespace App\Service;


use App\Entity\Order;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use function Symfony\Component\String\u;

class UserService
{
    private $userRepository;
    private $manager;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $manager)
    {
        $this->userRepository = $userRepository;
        $this->manager = $manager;
    }

    /**
     * @param string $username
     * @return User|null
     */
    public function readOneByUsername(string $username) {
        return $this->userRepository->findOneBy(["userName" => $username]);
    }

    /**
     * @return User[]
     */
    public function readAll() {
        return $this->userRepository->findAll();
    }

    /**
     * @param int $userId
     * @param Order $order
     * @throws \Exception
     * @return User
     */
    public function addOrder(int $userId, Order $order) {
        /** @var User $user */
        $user = $this->userRepository->find($userId);
        if ($user == null) throw new \Exception();

        $user->addOrder($order);

        $this->manager->flush();

        return $user;
    }

    public function create(User $user) {
        $this->manager->persist($user);
        $this->manager->flush();
    }
}