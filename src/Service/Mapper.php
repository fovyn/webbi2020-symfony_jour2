<?php


namespace App\Service;


class Mapper
{

    public function mapTo(string $target, string $src, $data) {
        try {
            $targetReflection = new \ReflectionClass($target);
            $srcReflection = new \ReflectionClass($src);

            $targetObj = new $target();

            $srcProperties = $srcReflection->getProperties();

            foreach ($srcProperties as $property) {
                if($targetReflection->hasProperty($property->getName())) {
                    $targetP = $targetReflection->getProperty($property->getName());
                    $targetP->setValue($targetObj, $property->getValue($data));
                }
            }

            return $targetObj;
        } catch (\ReflectionException $e) {
        }

    }
}